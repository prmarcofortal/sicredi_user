package cadastro;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class CadastroUsuario {

    private WebDriver navegador;

    @Before
    public void setUp() {
        //Abrindo o navegador
        System.setProperty("webdriver.chrome.driver", "C:\\Chrome Driver\\Diver 104.0.5112.79\\chromedriver.exe");
        navegador = new ChromeDriver();
        navegador.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        // 1.Acessar a página:  https://www.grocerycrud.com/v1.x/demo/my_boss_is_in_a_hurry/bootstrap
        navegador.get("https://www.grocerycrud.com/v1.x/demo/my_boss_is_in_a_hurry/bootstrap");
        navegador.manage().window().maximize();
    }

    @Test
    public void testcadastroUsuario() {
        navegador.findElement(By.id("switch-version-select")).click();

        // 2.Mudar o valor do combo Select version para “Bootstrap V4 Theme
        WebElement dropdown = navegador.findElement(By.id("switch-version-select"));
        dropdown.findElement(By.xpath("//option[. = 'Bootstrap V4 Theme']")).click();

        // 3.Clicar no botão Add Customer
        navegador.findElement(By.linkText("Add Record")).click();

        // 4.Preencher os campos do formulário com as seguintes informações:
        navegador.findElement(By.id("field-customerName")).sendKeys("Teste sincred");
        navegador.findElement(By.id("field-contactLastName")).sendKeys("Teste");
        navegador.findElement(By.id("field-contactFirstName")).sendKeys("Marco A. Ramos");
        navegador.findElement(By.id("field-phone")).sendKeys("51 9999-9999");
        navegador.findElement(By.id("field-addressLine1")).sendKeys("Avenida Asis Brasil, 370");
        navegador.findElement(By.id("field-addressLine2")).sendKeys("Torre D");
        navegador.findElement(By.id("field-city")).sendKeys("Porto Alegre");
        navegador.findElement(By.id("field-state")).sendKeys("RS");
        navegador.findElement(By.id("field-postalCode")).sendKeys("91000-000");
        navegador.findElement(By.id("field-country")).sendKeys("Brasil");
        navegador.findElement(By.id("field-salesRepEmployeeNumber")).sendKeys("00112");
        navegador.findElement(By.id("field-creditLimit")).sendKeys("200");

        // 5.Clicar no botão Save
        navegador.findElement(By.id("form-button-save")).click();

        // 6.Validar a mensagem “Your data has been successfully stored into the database.

        WebElement mensagemPop = navegador.findElement(By.xpath("//p[contains(text(), \"Your data has been successfully stored into the database.\")]"));
        String mensagem = mensagemPop.getText();
        assertEquals("Your data has been successfully stored into the database.", mensagem);

    }

    @After
    public void tearDown() {
        // 7.Fechar o browsernavegador.close();
        navegador.close();
    }
}
